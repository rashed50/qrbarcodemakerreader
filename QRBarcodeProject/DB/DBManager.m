//
//  DBManager.m
//  TeeTimes
//
//  Created by w3Admin on 1/30/14.
//  Copyright (c) 2014 TeeTimes.Net. All rights reserved.
//

#import "DBManager.h"


#import "FMDB.h"
#import "FMDatabaseQueue.h"


static NSString *databasePath;
static NSString *databaseName = @"QRCodeScan.db";
static NSString *table_history = @"history";
static sqlite3 *DocScanDatabase;
static NSString* fileDirectoryName =@"QRcode";


static DBManager *_instanceDBManager;


// table CONTACTS field list
static NSString *COLOUMN_HISTORY_ID = @"hid"; // int
static NSString *COLOUMN_SOURCE_TYPE = @"source_type";  // 0 = QRcode generate , 1 = QRcode scan
static NSString *COLOUMN_FILE_SOURCE_URL = @"file_url";
static NSString *COLOUMN_FILE_IMAGE_URL = @"image_url";
static NSString *COLOUMN_HISTORY_DATE = @"scan_date";


@implementation DBManager


+ (FMDatabaseQueue *)sharedManager
{
    [self createDirectoryIfNotExist];
    static FMDatabaseQueue *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init
{
    self = [super init];
    if (self) {
        _databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[self databasePath]];
    }
    return self;
}

- (NSString *)databasePath
{
    NSString *docsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    return [docsPath stringByAppendingPathComponent:databaseName];
}


+(DBManager *)sharedInstance
{
    if (_instanceDBManager == nil)
    {
        if([self openDatabaseConnection]!=NULL)
        {
           
            _instanceDBManager = [[self alloc]init];
            _instanceDBManager.yoDatabase1=DocScanDatabase;
            _instanceDBManager.databaseName1=databaseName;
            NSLog(@" database path from single ton class ====    %@",databasePath);
            _instanceDBManager.databasePath1=databasePath;
            // NSLog(@"database open successully completed");
            
            
             [self sharedManager];
        }
        else
        {
            _instanceDBManager=NULL;
            NSLog(@"Database connection  Error ");
        }
    }
    return _instanceDBManager;
}


+(sqlite3*)openDatabaseConnection
{
   
    
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = dirPaths[0];
    databasePath= [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent:databaseName]];
    NSLog(@"===== ==== ===== ===== ==== %@ ",databasePath);
    
    const char *dbpath = [databasePath UTF8String];
    if (DocScanDatabase==NULL)
    {
        
        if (sqlite3_open(dbpath, &DocScanDatabase) == SQLITE_OK)
        {
            
            NSLog(@" open database connection   ");
            return  DocScanDatabase;
        }
        else
            return NULL;
    }
    else
    {
        NSLog(@"  database  connection already open ");
        return DocScanDatabase;
    }
    return DocScanDatabase;
    
}

+(int)closeDatabaseConnection
{
    DBManager *manager=[[DBManager alloc]init];
    manager=[DBManager sharedInstance];
    sqlite3_close(manager.yoDatabase1);
        return 1;
    
    
}


// FOR CREATING DATABASE
+ (int) createDatabase
{
    DBManager *manager=[[DBManager alloc]init];
    manager=[DBManager sharedInstance];
    int statusOfAddingToDB = 0;
    
    char *errMsg;
 
     
    NSString *queryContactsSQL = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (%@ INTEGER PRIMARY KEY AUTOINCREMENT, %@ TEXT, %@ TEXT, %@ TEXT,%@ TEXT)",table_history,COLOUMN_HISTORY_ID,COLOUMN_SOURCE_TYPE,COLOUMN_FILE_SOURCE_URL,COLOUMN_HISTORY_DATE,COLOUMN_FILE_IMAGE_URL];
    const char *sql_contacts_stmt = [queryContactsSQL UTF8String];
    
            if (sqlite3_exec(manager.yoDatabase1, sql_contacts_stmt, NULL, NULL, &errMsg) == SQLITE_OK)
            {
                statusOfAddingToDB = 1;
            }
            
   
    
    return statusOfAddingToDB;
}

+(void)addHistoryInfo:(NSMutableDictionary *)inputDict;
{
    
    DBManager *manager=[[DBManager alloc]init];
    manager=[DBManager sharedInstance];
    
    //DatabaseManager *databaseManager = [DatabaseManager sharedManager];
    
    [manager.databaseQueue inDatabase:^(FMDatabase *db) {
        
        
        
        if ([db open])
        {
            
            NSString  *insertSQL =[NSString stringWithFormat:@"INSERT INTO %@ ( %@ , %@ ,%@,%@) VALUES (?,?,?,?)",table_history,COLOUMN_SOURCE_TYPE,COLOUMN_FILE_SOURCE_URL,COLOUMN_HISTORY_DATE,COLOUMN_FILE_IMAGE_URL ];
 
            
            BOOL result =  [db executeUpdate:insertSQL,[inputDict objectForKey:COLOUMN_SOURCE_TYPE],
                            [inputDict objectForKey:COLOUMN_FILE_SOURCE_URL],
                            [inputDict objectForKey:COLOUMN_HISTORY_DATE],
                            [inputDict objectForKey:COLOUMN_FILE_IMAGE_URL]];
            if (result)
            {
                NSLog(@"save data ");
            }
            
            [db close];
        }
        else {
            NSLog(@"Error: \(contactDB.lastErrorMessage())");
        }
        
        
    }];

}


+(NSMutableArray *)getAllHistory
{
    NSMutableArray *alldata=[[NSMutableArray alloc]init];
    DBManager *manager=[[DBManager alloc]init];
    manager=[DBManager sharedInstance];
    
    
    FMDatabase *contactDB = [FMDatabase databaseWithPath:manager.databasePath1];
    
    if ([contactDB open])
    {
        NSString *querySQL =[NSString stringWithFormat:@"select * from %@",table_history];
        
         
        FMResultSet *result=  [contactDB executeQuery:querySQL withArgumentsInArray:nil];
        while([result next]== true)
        {
            
            NSMutableDictionary *list = [[NSMutableDictionary alloc] init];
            
            
            [list setValue:[result stringForColumn:COLOUMN_HISTORY_ID] forKey:COLOUMN_HISTORY_ID];
            [list setValue:[result stringForColumn:COLOUMN_SOURCE_TYPE] forKey:COLOUMN_SOURCE_TYPE];
            [list setValue:[result stringForColumn:COLOUMN_FILE_SOURCE_URL] forKey:COLOUMN_FILE_SOURCE_URL];
            [list setValue:[result stringForColumn:COLOUMN_HISTORY_DATE] forKey:COLOUMN_HISTORY_DATE];
            [list setValue:[result stringForColumn:COLOUMN_FILE_IMAGE_URL] forKey:COLOUMN_FILE_IMAGE_URL];
            
            [alldata addObject:list];
             
        }
    }
    return alldata;
}

+(NSMutableArray *)getHistoryUsingType: (NSString *)sourceType
{
    NSMutableArray *alldata=[[NSMutableArray alloc]init];
    DBManager *manager=[[DBManager alloc]init];
    manager=[DBManager sharedInstance];
    
    
    FMDatabase *contactDB = [FMDatabase databaseWithPath:manager.databasePath1];
    
    if ([contactDB open])
    {
        NSString *querySQL =[NSString stringWithFormat:@"select * from %@ WHERE source_type = %@",table_history,sourceType];
        
        
        FMResultSet *result=  [contactDB executeQuery:querySQL withArgumentsInArray:nil];
        while([result next]== true)
        {
            
            NSMutableDictionary *list = [[NSMutableDictionary alloc] init];
            
            
            [list setValue:[result stringForColumn:COLOUMN_HISTORY_ID] forKey:COLOUMN_HISTORY_ID];
            [list setValue:[result stringForColumn:COLOUMN_SOURCE_TYPE] forKey:COLOUMN_SOURCE_TYPE];
            [list setValue:[result stringForColumn:COLOUMN_FILE_SOURCE_URL] forKey:COLOUMN_FILE_SOURCE_URL];
            [list setValue:[result stringForColumn:COLOUMN_HISTORY_DATE] forKey:COLOUMN_HISTORY_DATE];
            [list setValue:[result stringForColumn:COLOUMN_FILE_IMAGE_URL] forKey:COLOUMN_FILE_IMAGE_URL];
            
            [alldata addObject:list];
            
        }
    }
    return alldata;
}



+(int)deleteAllHistory
{
    
    DBManager *manager=[[DBManager alloc]init];
    manager=[DBManager sharedInstance];
    
    
    FMDatabase *contactDB = [FMDatabase databaseWithPath:manager.databasePath1];
    
    if ([contactDB open])
    {
        NSString *querySQL = [NSString stringWithFormat:@"DELETE FROM %@",table_history];
        FMResultSet *result=  [contactDB executeQuery:querySQL withArgumentsInArray:nil];
        
        if (result) {
            return 1;
        }
    }
    return 0;
}




+(NSString*)GetSourceTypeColoumName
{
    return COLOUMN_SOURCE_TYPE;
}
+(NSString*)GetScanDateColoumName
{
    return COLOUMN_HISTORY_DATE;
}
+(NSString*)GetSourceUrlColoumName
{
    return COLOUMN_FILE_SOURCE_URL;
}
+(NSString*)GetImageFileURlColoumName
{
    return COLOUMN_FILE_IMAGE_URL;
}


+(void)createDirectoryIfNotExist
{
     NSString *folderName = [NSString stringWithFormat:@"/%@",fileDirectoryName];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *filePath =[documentsDirectory stringByAppendingPathComponent:folderName];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:filePath])
        [[NSFileManager defaultManager] createDirectoryAtPath:filePath withIntermediateDirectories:NO attributes:nil error:&error];
    
   
}

+(NSString*)getFileDirectoryFullPath:(NSString*)directoryPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *filePath =[documentsDirectory stringByAppendingPathComponent:directoryPath];
    return  filePath;
}


+(NSString*)getFileDirectoryPath:(NSString*)fileName
{
    return [NSString stringWithFormat:@"%@/%@.png",fileDirectoryName,fileName];
}

+(NSString*)saveImageToPath:(UIImage*)img
{
    
    NSString *path = [self getFileDirectoryPath:[NSString stringWithFormat:@"test_%d",(int)[[self getAllHistory] count]]];
   NSString *fullpath =[self getFileDirectoryFullPath:path];
    NSData *data = UIImageJPEGRepresentation(img, 1);
    [data writeToFile:fullpath atomically:YES];
    
    return path;
    
}


+(NSString*)getcurrentDate
{
    NSDate *todayDate = [NSDate date]; // get today date
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date..
    [dateFormatter setDateFormat:@"dd-MM-yyyy"]; //Here we can set the format which we need
    NSString *convertedDateString = [dateFormatter stringFromDate:todayDate];// here convert date in
    NSLog(@"Today formatted date is %@",convertedDateString);
    
    return convertedDateString;
}

@end

