//
//  AppDelegate.h
//  QR Code P2
//
//  Created by SoftKnight on 12/8/15.
//  Copyright © 2015 SoftKnight. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

