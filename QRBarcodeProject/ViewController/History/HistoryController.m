//
//  HistoryController.m
//  QR Code Generator
//
//  Created by rashed hoque on 12/8/15.
//  Copyright © 2015 softknight. All rights reserved.
//

#import "HistoryController.h"
#import "DBManager.h"
#import "PreviewViewController.h"
#import "QRBarcodeProject-Swift.h"
@interface HistoryController ()

@end

@implementation HistoryController{
    
    BOOL isFiltered;
    NSMutableArray *filteredTableData;
    UIImage *historyDataImage;
    NSString *qrDataString;
    ShareActionSheet *shareActionSheetObj;
    NSMutableDictionary *historyData ;
    UILabel *dataLabel, *dateLabel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    shareActionSheetObj = [[ShareActionSheet alloc]
                           init];
    shareActionSheetObj.shareDelegate = self;
    _searchBar.delegate = self;
    [_segmentControl setTitleTextAttributes:@{NSForegroundColorAttributeName : [Helper getAppColor]} forState:UIControlStateSelected];
     self.view.backgroundColor = [UIColor clearColor];
     self.view.backgroundColor = [UIColor whiteColor];
    
    [Helper setNavigationBarPropertyWithNavbar:self.navigationController size:18 title:@"Barcode History"];
   // _segmentControl.tintColor =  [Helper getAppColor];
    
       
}



-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self initialization:@"1"];
    
  
    
}





-(void)initialization:(NSString *)type {
  
    NSLog(@"%ld",(long)_segmentControl.selectedSegmentIndex);
    
    [historyArray removeAllObjects];
    historyArray = [DBManager getHistoryUsingType:type];
    
    [Tableviewoutlet reloadData];

}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    _segmentControl.selectedSegmentIndex = 0;
}

#pragma mark - searchbar Delegate Method -
-(void) searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
        searchBar.showsCancelButton = YES;
}

-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{

    if(text.length == 0)
    {
        isFiltered = FALSE;
    }
    else
    {
        isFiltered = true;
        filteredTableData = [[NSMutableArray alloc] init];
        
        
        NSString *dateString = text;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        // this is imporant - we set our input date format to match our input string
        // if format doesn't match you'll get nil from your string, so be careful
        [dateFormatter setDateFormat:@"dd-MM-YYYY"];
        NSDate *dateFromString = [[NSDate alloc] init];
        dateFromString = [dateFormatter dateFromString:dateString];
        
        
        
        if (dateFormatter !=nil)
        {
            for (NSDictionary* data in historyArray)
            {
                NSRange nameRange = [[data objectForKey:@"scan_date"] rangeOfString:text options:NSCaseInsensitiveSearch];
                NSRange descriptionRange = [data.description rangeOfString:text options:NSCaseInsensitiveSearch];
                if(nameRange.location != NSNotFound || descriptionRange.location != NSNotFound)
                {
                    [filteredTableData addObject:data];
                }
            }
        }
        else
        {
            for (NSDictionary* data in historyArray)
            {
                NSRange nameRange = [[data objectForKey:[DBManager GetSourceUrlColoumName]] rangeOfString:text options:NSCaseInsensitiveSearch];
                NSRange descriptionRange = [data.description rangeOfString:text options:NSCaseInsensitiveSearch];
                if(nameRange.location != NSNotFound || descriptionRange.location != NSNotFound)
                {
                    [filteredTableData addObject:data];
                }
            }
            
            
        }
        
        
        
    }
    
    [Tableviewoutlet reloadData];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    searchBar.text =@"";
    isFiltered = NO;
    searchBar.showsCancelButton = NO;
    [searchBar resignFirstResponder];
    [Tableviewoutlet reloadData];
}



#pragma mark - Table view Delegate Method -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSUInteger rowCount;
    if(isFiltered)
        rowCount = (int) filteredTableData.count;
    else
        rowCount =(int) historyArray.count;
    
    
    return rowCount;    //count number of row from counting array hear cataGorry is An Array
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 80;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"MyIdentifier";
    
    NSMutableDictionary *infoDic = [historyArray objectAtIndex:indexPath.row];
    
    if(isFiltered)
        infoDic = [filteredTableData objectAtIndex:indexPath.row];
    else
        infoDic = [historyArray objectAtIndex:indexPath.row];
 
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
   // if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:MyIdentifier] ;
    
    }
    UIImageView *QRImageView = [[UIImageView alloc] init];
    QRImageView.frame = CGRectMake(10, 10, 60, 60);
    QRImageView.image = [UIImage imageWithContentsOfFile:[DBManager getFileDirectoryFullPath:[infoDic objectForKey:[DBManager GetImageFileURlColoumName]]]];
    QRImageView.contentMode = UIViewContentModeScaleAspectFill;
    QRImageView.clipsToBounds = YES;
    
     dataLabel = [[UILabel alloc] init];
    dataLabel.frame = CGRectMake(85, 10, 200, 25);
    dataLabel.text = [infoDic objectForKey:[DBManager GetSourceUrlColoumName]];
    
    dateLabel = [[UILabel alloc] init];
    dateLabel.frame = CGRectMake(85, 40, 200, 25);
    dateLabel.text = [infoDic objectForKey:[DBManager GetScanDateColoumName]];
    
    [cell addSubview:QRImageView];
    [cell addSubview:dataLabel];
    [cell addSubview:dateLabel];
    

    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    historyData = [[NSMutableDictionary alloc] init];
    historyData = [historyArray objectAtIndex:indexPath.row];
    
    qrDataString = [historyData objectForKey:[DBManager GetScanDateColoumName]];
    historyDataImage = [UIImage imageWithContentsOfFile:[DBManager getFileDirectoryFullPath:[historyData objectForKey:[DBManager GetImageFileURlColoumName]]]];
    [self OpenAcionSheet];
    //[Tableviewoutlet deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Segment Control Action -

-(IBAction) segmentedControlIndexChanged{
      
    switch (_segmentControl.selectedSegmentIndex) {
        case 0:
           
            [self initialization:@"1"];
            NSLog(@"Generate Button Clicked!!");
            break;
        case 1:
            [self initialization:@"2"];
            NSLog(@"Scan Button Clicked!!");
            break;
            
        default:
            break;
    }
    
}

#pragma mark - ActionSheet Delegate Method -

-(void)OpenAcionSheet{
     
       UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Share Options:"
                                   message:@""
                                   preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* preview = [UIAlertAction actionWithTitle:@"Preview Barcode" style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action) {
                [self previewAction];
                                                       
                                       }];

        UIAlertAction* fbAction = [UIAlertAction actionWithTitle:@"Share on Facebook" style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
            [self->shareActionSheetObj ShareToSocialMedia:1];
                                                    
                                    }];
        UIAlertAction* twitterAction = [UIAlertAction actionWithTitle:@"Share on Twitter" style:UIAlertActionStyleDefault
        handler:^(UIAlertAction * action) {
            
            
            [self->shareActionSheetObj ShareToSocialMedia:2];
       
        }];
        
        UIAlertAction* emailAction = [UIAlertAction actionWithTitle:@"E-mail" style:UIAlertActionStyleDefault
        handler:^(UIAlertAction * action) {
            NSData *ImageData = UIImageJPEGRepresentation(self->historyDataImage, 1);
            [self->shareActionSheetObj SendByEmail:ImageData];
        }];
        
        
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
        handler:^(UIAlertAction * action) {}];

        [alert addAction:cancel];
        [alert addAction:preview];
        [alert addAction:fbAction];
        [alert addAction:twitterAction];
        [alert addAction:emailAction];
        
       if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
             [self presentViewController:alert animated:YES completion:nil];
        }
         else {
             UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:alert];
            [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }
}


#pragma mark - Share Action Sheet Delegate -
-(void) socialShareViewOpen: (SLComposeViewController*) composeController{
    [composeController addImage:historyDataImage];
    [self presentViewController:composeController animated:YES completion:nil];
}

-(void) socialShareViewClose: (SLComposeViewController*) composeController{
    
    [composeController dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)sendMailCompose:(MFMailComposeViewController *)mailCont
{
    [self presentViewController:mailCont animated:YES completion:Nil];
}

-(void) closeMailComposeView
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)previewAction{
    
    dispatch_async(dispatch_get_main_queue(), ^ {
        
        PreviewViewController *preViewObj = [self.storyboard instantiateViewControllerWithIdentifier:@"previewViewController"];
        preViewObj.previewInfoDic  = self->historyData;
       
        [self presentViewController:preViewObj animated:YES completion:nil ];
    });
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
