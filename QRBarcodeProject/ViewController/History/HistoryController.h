//
//  HistoryController.h
//  QR Code Generator
//
//  Created by rashed hoque on 12/8/15.
//  Copyright © 2015 softknight. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShareActionSheet.h"

@interface HistoryController : UIViewController< UIGestureRecognizerDelegate,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,SocialShareDelegate>

{
  

    IBOutlet UITableView *Tableviewoutlet;

    NSMutableArray *historyArray; 

}

-(IBAction) segmentedControlIndexChanged;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;

@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentControl;

 

@end
