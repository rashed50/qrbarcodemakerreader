//
//  ViewController.h
//  QR Code P2
//
//  Created by SoftKnight on 12/8/15.
//  Copyright © 2015 SoftKnight. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "ShareActionSheet.h"


@interface ViewController : UIViewController<UITextFieldDelegate,MFMailComposeViewControllerDelegate,UIActionSheetDelegate,SocialShareDelegate,UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *InfoLbl;
@property (strong, nonatomic) IBOutlet UITextField *insertTextField;
@property (strong, nonatomic) IBOutlet UIImageView *qrCodeImageView;

@property (strong, nonatomic) IBOutlet UIButton *generateButton;
@property (strong, nonatomic) IBOutlet UIButton *shareButton;


- (IBAction)generateQRCodeButtonAction:(id)sender;
- (IBAction)shareButtonAction:(id)sender;



@end

