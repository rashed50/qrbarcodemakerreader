//
//  ViewController.m
//  QR Code P2
//
//  Created by SoftKnight on 12/8/15.
//  Copyright © 2015 SoftKnight. All rights reserved.
//

#import "ViewController.h"
#import "UIImage+MDQRCode.h"
#import "ScanViewController.h"
#import "DBManager.h"
//#import "QRBarcodeProject/QRBarcodeProject-Swift.h"
#import "QRBarcodeProject-Swift.h"
//#import "rashed.barcodemakerandreader-Swift.h"

// create barcode
#import "Code39.h"

@interface ViewController ()

@end

@implementation ViewController
{
    ShareActionSheet *shareActionSheetObj;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
   // [Helper setn]
    [self clearUIData];
    shareActionSheetObj = [[ShareActionSheet alloc] init];
    shareActionSheetObj.shareDelegate = self;
    [self.view setBackgroundColor:[UIColor clearColor]];
    _insertTextField.delegate = self;
    _generateButton.layer.cornerRadius = 3;
    _generateButton.clipsToBounds = YES;
    _shareButton.layer.cornerRadius = 3;
    _shareButton.clipsToBounds = YES;

    [Helper setNavigationBarPropertyWithNavbar:self.navigationController size:18 title:@"Generate New Barcode"];
    [Helper setUIButtonPropertyWithButton:_generateButton title:@""];
    [Helper setUIButtonPropertyWithButton:_shareButton title:@""];
   // [Helper setTabBarPropertyWithTabBar:self.tabBarController];
    [DBManager sharedInstance];
    [DBManager createDatabase];
    
    
    
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(hidekeyBoard)];
    singleFingerTap.numberOfTapsRequired = 1;
    singleFingerTap.delegate = self;
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self.view addGestureRecognizer:singleFingerTap];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   // [self clearUIData];

 
    
}

-(void)hidekeyBoard
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    NSLog(@"textFieldDidBeginEditing");
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    NSLog(@"textFieldShouldReturn:");
    if (textField.tag == 1) {
        UITextField *passwordTextField = (UITextField *)[self.view viewWithTag:2];
        [passwordTextField becomeFirstResponder];
    }
    else {
        [textField resignFirstResponder];
    }
    return YES;
}

- (IBAction)generateQRCodeButtonAction:(id)sender {
    
    if( [_insertTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] != nil &&  ![[_insertTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""] )
    {
       // NSString *qrCodeString = _insertTextField.text;
        //_qrCodeImageView.image = [UIImage mdQRCodeForString:qrCodeString size:_qrCodeImageView.bounds.size.width fillColor:[UIColor darkGrayColor]];
        int barcode_width = _qrCodeImageView.frame.size.width;
        int barcode_height = _qrCodeImageView.frame.size.height/2;
        UIImage *code39Image;
        if ([_insertTextField.text length]>0)
        {
            
            code39Image = [Code39 code39ImageFromString:_insertTextField.text Width:barcode_width Height:barcode_height];
            
            _qrCodeImageView.image = code39Image;
            _qrCodeImageView.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        
    }
    else
    {
       
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Error"
                                      message:@"Please Enter text  and then generate Barcode "
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel
        handler:^(UIAlertAction * action) {}];

        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    
   
 
}

- (IBAction)shareButtonAction:(id)sender {
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Share Options:"
                               message:@""
                               preferredStyle:UIAlertControllerStyleActionSheet];

    UIAlertAction* fbAction = [UIAlertAction actionWithTitle:@"Share on Facebook" style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
        [self->shareActionSheetObj ShareToSocialMedia:1];
                                                   [self clearUIData];
                                }];
    UIAlertAction* twitterAction = [UIAlertAction actionWithTitle:@"Share on Twitter" style:UIAlertActionStyleDefault
    handler:^(UIAlertAction * action) {
        
        
        [self->shareActionSheetObj ShareToSocialMedia:2];
        [self clearUIData];
    }];
    
    UIAlertAction* emailAction = [UIAlertAction actionWithTitle:@"E-mail" style:UIAlertActionStyleDefault
    handler:^(UIAlertAction * action) {
        NSData *ImgData = UIImageJPEGRepresentation(self->_qrCodeImageView.image, 1);
        [self->shareActionSheetObj SendByEmail:ImgData];
        [self clearUIData];
    }];
    
    UIAlertAction* saveAction = [UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault
    handler:^(UIAlertAction * action) {
        [self saveScanData];
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
    handler:^(UIAlertAction * action) {}];

    [alert addAction:cancel];
    [alert addAction:fbAction];
    [alert addAction:twitterAction];
    [alert addAction:emailAction];
    [alert addAction:saveAction];
    
 
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
         [self presentViewController:alert animated:YES completion:nil];
    }
     else {
         UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:alert];
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    
 }
 

#pragma mark - Share Action Sheet Delegate -
-(void) socialShareViewOpen: (SLComposeViewController*) composeController{
    [composeController addImage:_qrCodeImageView.image];
    // [fbController setp];
    [self presentViewController:composeController animated:YES completion:nil];
}

-(void) socialShareViewClose: (SLComposeViewController*) composeController{
    
    [composeController dismissViewControllerAnimated:YES completion:nil];

}

-(void)sendMailCompose:(MFMailComposeViewController *)mailCont
{
    
    [self presentViewController:mailCont animated:YES completion:Nil];
}

-(void) closeMailComposeView
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)clearUIData
{
    _qrCodeImageView.image = [UIImage imageNamed:@"placeholder-100.png"];
    _qrCodeImageView.contentScaleFactor = UIViewContentModeScaleAspectFit;
    _qrCodeImageView.clipsToBounds = YES;
    _insertTextField.text =@"";

}
-(void) saveScanData{
   
        
        if(_qrCodeImageView.image !=nil && [_insertTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] != nil &&  ![[_insertTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""] )
        {
        
        
        NSMutableDictionary *infoDic = [[NSMutableDictionary alloc]init];
        UIImage *img = _qrCodeImageView.image;
        
        [infoDic setObject:@"1" forKey:[DBManager GetSourceTypeColoumName]];
        [infoDic setObject:_insertTextField.text forKey:[DBManager GetSourceUrlColoumName]];
        [infoDic setObject: [DBManager getcurrentDate] forKey:[DBManager GetScanDateColoumName]];
        [infoDic setObject:[DBManager saveImageToPath:img] forKey:[DBManager GetImageFileURlColoumName]];
        
        [DBManager addHistoryInfo:infoDic];
        UIImageWriteToSavedPhotosAlbum(img, nil, nil, nil);
        [self clearUIData];
             NSLog(@"Scan data saved!!!!");
        
    
    }
    
    
}

@end
