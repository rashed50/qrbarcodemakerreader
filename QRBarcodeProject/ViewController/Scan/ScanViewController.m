//
//  ScanViewController.m
//  QR Code P2
//
//  Created by SoftKnight on 12/8/15.
//  Copyright © 2015 SoftKnight. All rights reserved.
//

#import "ScanViewController.h"
#import <Social/Social.h>
#import <AVFoundation/AVAudioPlayer.h>
#import "DBManager.h"
#import "QRBarcodeProject-Swift.h"



//BOOL isScanSuccessfull;

@interface ScanViewController ()

@end

@implementation ScanViewController{
    
    ShareActionSheet *shareActionSheetObj;
    AVAudioPlayer *audioPlayer;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    _resultImage.image = [UIImage imageNamed:@"placeholder-100.png"];
    _scanButton.layer.cornerRadius = 3;
    _scanButton.clipsToBounds = YES;
    _shareButton.layer.cornerRadius = 3;
    _shareButton.clipsToBounds = YES;
    shareActionSheetObj = [[ShareActionSheet alloc] init];
    shareActionSheetObj.shareDelegate = self;
    _resultText.text = @"";
    [Helper setNavigationBarPropertyWithNavbar:self.navigationController size:18 title:@"Scan Barcode"];
    [Helper setUIButtonPropertyWithButton:_scanButton title:@""];
    [Helper setUIButtonPropertyWithButton:_shareButton title:@""];
   // [Helper setTabBarPropertyWithTabBar:self.tabBarController];
    _scanResultMessageLbl.textColor = [Helper getAppColor];
    [self setUI:true];
    [self loadAudioClip];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
  //  [self clearUIData];
}
 

-(void)setUI:(BOOL)isHidden{
    
    _scanResultMessageLbl.hidden = isHidden;
    _resultImage.hidden = isHidden;
    if(!isHidden)
       _resultText.text = @"";
}

-(void)clearUIData
{
    _resultImage.image = [UIImage imageNamed:@"placeholder-100.png"];
    _resultText.text =@"";
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)takePhotoFromGallery:(UIButton *)sender {

    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;

    [self presentViewController:picker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {

    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)openCameraView
{
     
   
     
  
    
}

-(void)openAlertViewForScan{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Choose Options"
                               message:@""
                               preferredStyle:UIAlertControllerStyleActionSheet];

    UIAlertAction* cameraAction = [UIAlertAction actionWithTitle:@"Open Camera" style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                [self openCameraView];
                                }];
    UIAlertAction* galleryAction = [UIAlertAction actionWithTitle:@"From Gallery" style:UIAlertActionStyleDefault
    handler:^(UIAlertAction * action) {}];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
    handler:^(UIAlertAction * action) {}];

    [alert addAction:cancel];
    [alert addAction:cameraAction];
    [alert addAction:galleryAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}


- (IBAction) scanButtonTapped
{
    
   // [self openAlertViewForScan];
/*
    CGSize siz  = CGSizeMake(500, 200);
    ZBarImage *zimg = [[ZBarImage alloc] initWithCGImage:(__bridge CGImageRef)([UIImage imageNamed:@"14.png"]) size:siz];
    ZBarImageScanner *scandoc = [[ZBarImageScanner alloc]init];
    NSInteger resultsnumber = [scandoc scanImage:zimg];
    if(resultsnumber > 0){
        ZBarSymbolSet *results = scandoc.results;
        //Here you will get result.
        _resultText.text = results.description;
    }
   // [Helper readBarcodeFromGalleryPhotoWithImg:[UIImage imageNamed:@"13.png"]];
 
    NSLog(@"TBD: scan barcode here...");
    ZBarReaderViewController *reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    reader.supportedOrientationsMask = ZBarOrientationMaskAll;
  
    if([ZBarReaderController isSourceTypeAvailable:
                             UIImagePickerControllerSourceTypeCamera])
    reader.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    ZBarImageScanner *scanner = reader.scanner;
    // TODO: (optional) additional reader configuration here
    
    // EXAMPLE: disable rarely used I2/5 to improve performance
    [scanner setSymbology: ZBAR_I25
                   config: ZBAR_CFG_ENABLE
                       to: 0];
    
    [self setUI:true];
    [self presentViewController:reader animated:YES completion:nil];
 */
  
    
}

- (IBAction) shareButtonTapped
{
    
    
   UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Share Options:"
                               message:@""
                               preferredStyle:UIAlertControllerStyleActionSheet];

    UIAlertAction* fbAction = [UIAlertAction actionWithTitle:@"Share on Facebook" style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
        [self->shareActionSheetObj ShareToSocialMedia:1];
                                                   [self clearUIData];
                                }];
    UIAlertAction* twitterAction = [UIAlertAction actionWithTitle:@"Share on Twitter" style:UIAlertActionStyleDefault
    handler:^(UIAlertAction * action) {
        
        
        [self->shareActionSheetObj ShareToSocialMedia:2];
        [self clearUIData];
    }];
    
    UIAlertAction* emailAction = [UIAlertAction actionWithTitle:@"E-mail" style:UIAlertActionStyleDefault
    handler:^(UIAlertAction * action) {
        NSData *Imgdata = UIImageJPEGRepresentation(self->_resultImage.image, 1);
        [self->shareActionSheetObj SendByEmail:Imgdata];
        [self clearUIData];
    }];
    
    UIAlertAction* saveAction = [UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault
    handler:^(UIAlertAction * action) {
        [self saveScanData];
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
    handler:^(UIAlertAction * action) {}];

    [alert addAction:cancel];
    [alert addAction:fbAction];
    [alert addAction:twitterAction];
    [alert addAction:emailAction];
    [alert addAction:saveAction];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
         [self presentViewController:alert animated:YES completion:nil];
    }
     else {
         UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:alert];
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
      
    
}

 


-(void) saveScanData{
    NSLog(@"Scan data saved!!!!  %@",_resultText.text);

        if( [_resultText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] != nil &&  ![[_resultText.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""] )
        {
        
        NSMutableDictionary *infoDic = [[NSMutableDictionary alloc]init];
        UIImage *img = _resultImage.image;
        
        [infoDic setObject:@"2" forKey:[DBManager GetSourceTypeColoumName]];
        [infoDic setObject:_resultText.text forKey:[DBManager GetSourceUrlColoumName]];
        [infoDic setObject: [DBManager getcurrentDate] forKey:[DBManager GetScanDateColoumName]];
        [infoDic setObject:[DBManager saveImageToPath:img] forKey:[DBManager GetImageFileURlColoumName]];
        
        [DBManager addHistoryInfo:infoDic];
        UIImageWriteToSavedPhotosAlbum(img, nil, nil, nil);
        [self clearUIData];
                
        
    }

}

#pragma mark - Share Action Sheet Delegate -
-(void) socialShareViewOpen: (SLComposeViewController*) composeController{
    [composeController addImage:_resultImage.image];
    // [fbController setp];
    [self presentViewController:composeController animated:YES completion:nil];
}

-(void) socialShareViewClose: (SLComposeViewController*) composeController{
    
    [composeController dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)sendMailCompose:(MFMailComposeViewController *)mailCont
{
    [self presentViewController:mailCont animated:YES completion:Nil];
}

-(void) closeMailComposeView
{
    [self dismissViewControllerAnimated:YES completion:nil];
}




-(void)loadAudioClip
{
    NSBundle* bundle = [NSBundle mainBundle];
    NSString *graffitiSprayFile = [bundle pathForResource:@"beep-beep" ofType:@"aiff"];
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:graffitiSprayFile] error:NULL];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
