//
//  ScanViewController.h
//  QR Code P2
//
//  Created by SoftKnight on 12/8/15.
//  Copyright © 2015 SoftKnight. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBarSDK.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "ShareActionSheet.h"

@interface ScanViewController : UIViewController <ZBarReaderDelegate,MFMailComposeViewControllerDelegate,NSObject, UIActionSheetDelegate,SocialShareDelegate>

@property (strong, nonatomic) IBOutlet UIButton *scanButton;
@property (weak, nonatomic) IBOutlet UILabel *scanResultMessageLbl;

@property (strong, nonatomic) IBOutlet UIButton *shareButton;
@property (nonatomic, retain) IBOutlet UIImageView *resultImage;
@property (nonatomic, retain) IBOutlet UITextView *resultText;
- (IBAction) scanButtonTapped;
- (IBAction) shareButtonTapped;


@end
