//
//  PreviewViewController.h
//  QR Code P2
//
//  Created by SoftKnight on 12/12/15.
//  Copyright © 2015 SoftKnight. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShareActionSheet.h"
@interface PreviewViewController : UIViewController <SocialShareDelegate, UIActionSheetDelegate>

@property (strong, nonatomic) NSMutableDictionary *previewInfoDic;

@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (strong, nonatomic) IBOutlet UIImageView *preViewImageView;
@property (strong, nonatomic) IBOutlet UITextView *previewtextView;
- (IBAction)shareButtonAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *backButtonAction;
- (IBAction)backButtonAction:(id)sender;





@end
