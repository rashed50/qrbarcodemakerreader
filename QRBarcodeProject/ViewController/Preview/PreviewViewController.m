//
//  PreviewViewController.m
//  QR Code P2
//
//  Created by SoftKnight on 12/12/15.
//  Copyright © 2015 SoftKnight. All rights reserved.
//

#import "PreviewViewController.h"
#import "DBManager.h"
#import "QRBarcodeProject-Swift.h"

@interface PreviewViewController ()

@end

@implementation PreviewViewController{
    ShareActionSheet *shareActionSheetObj;
    NSString *qrDataString;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    shareActionSheetObj = [[ShareActionSheet alloc] init];
    shareActionSheetObj.shareDelegate = self;
    _backButtonAction.layer.cornerRadius = 3;
    _backButtonAction.clipsToBounds = YES;
    
    _previewtextView.text = [_previewInfoDic objectForKey:[DBManager GetSourceUrlColoumName]];
    _preViewImageView.image = [UIImage imageWithContentsOfFile:[DBManager getFileDirectoryFullPath:[_previewInfoDic objectForKey:[DBManager GetImageFileURlColoumName]]]];
    _preViewImageView.contentMode = UIViewContentModeScaleAspectFit;
    _preViewImageView.clipsToBounds = YES;
     self.view.backgroundColor = [UIColor whiteColor];
    [Helper setUIButtonPropertyWithButton:self.shareButton title:@""];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)shareButtonAction:(id)sender {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Share Options:"
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleActionSheet];
       

           UIAlertAction* fbAction = [UIAlertAction actionWithTitle:@"Share on Facebook" style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action) {
               [self->shareActionSheetObj ShareToSocialMedia:1];
                                                       
                                       }];
           UIAlertAction* twitterAction = [UIAlertAction actionWithTitle:@"Share on Twitter" style:UIAlertActionStyleDefault
           handler:^(UIAlertAction * action) {
               
               
               [self->shareActionSheetObj ShareToSocialMedia:2];
          
           }];
           
           UIAlertAction* emailAction = [UIAlertAction actionWithTitle:@"E-mail" style:UIAlertActionStyleDefault
           handler:^(UIAlertAction * action) {
               NSData *Imgdata = UIImageJPEGRepresentation(self->_preViewImageView.image, 1);
               [self->shareActionSheetObj SendByEmail:Imgdata];
           }];
           
           
           
           UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
           handler:^(UIAlertAction * action) {}];

           [alert addAction:cancel];
 
           [alert addAction:fbAction];
           [alert addAction:twitterAction];
           [alert addAction:emailAction];
           
          if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                [self presentViewController:alert animated:YES completion:nil];
           }
            else {
                UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:alert];
               [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
           }

}
 

#pragma mark - Share Action Sheet Delegate -
-(void) socialShareViewOpen: (SLComposeViewController*) composeController{
    [composeController addImage:_preViewImageView.image];
    // [fbController setp];
    [self presentViewController:composeController animated:YES completion:nil];
}

-(void) socialShareViewClose: (SLComposeViewController*) composeController{
    
    [composeController dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)sendMailCompose:(MFMailComposeViewController *)mailCont
{
    
    [self presentViewController:mailCont animated:YES completion:Nil];
}

-(void) closeMailComposeView
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)backButtonAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
