//
//  ShareActionSheet.m
//  QR Code P2
//
//  Created by SoftKnight on 12/9/15.
//  Copyright © 2015 SoftKnight. All rights reserved.
//

#import "ShareActionSheet.h"

@implementation ShareActionSheet


- (void)ShareToSocialMedia:(int)shareType 
{
    SLComposeViewController *fbController;
    
    NSString *msg =@"This is share using QR code generate and scanner iphone apps for testing purpose ";
    if (shareType == 1)
    {
        fbController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        SLComposeViewControllerCompletionHandler __block completionHandler=^(SLComposeViewControllerResult result)
        {
            
            [fbController dismissViewControllerAnimated:YES completion:nil];
            
            switch(result)
            {
                case SLComposeViewControllerResultCancelled:
                default:
                {
                    NSLog(@"Cancelled.....");
                    // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs://"]];
                    
                }
                    break;
                case SLComposeViewControllerResultDone:
                {
                    NSLog(@"Posted....");
                }
                    break;
            }};
        
        
        
        [fbController setInitialText:msg];
        
        
        [fbController setCompletionHandler:completionHandler];
//        [fbController addImage:_qrCodeImageView.image];
//        // [fbController setp];
//        [self presentViewController:fbController animated:YES completion:nil];
        [self.shareDelegate socialShareViewOpen:fbController];
        
        
    }
    else if (shareType == 2)
    {
        fbController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        SLComposeViewControllerCompletionHandler __block completionHandler=^(SLComposeViewControllerResult result)
        {
            
            [fbController dismissViewControllerAnimated:YES completion:nil];
            
            switch(result){
                case SLComposeViewControllerResultCancelled:
                default:
                {
                    NSLog(@"Cancelled.....");
                    // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs://"]];
                    
                }
                    break;
                case SLComposeViewControllerResultDone:
                {
                    NSLog(@"Posted....");
                }
                    break;
            }};
        
        
        [fbController setInitialText:msg];
       // [fbController addImage:_qrCodeImageView.image];
        
        [fbController setCompletionHandler:completionHandler];
       // [self presentViewController:fbController animated:YES completion:nil];
         [self.shareDelegate socialShareViewOpen:fbController];
        
        
    }
    
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sign in!" message:@"Please first Sign In!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}


-(void)SendByEmail:(NSData*)ImgData
{
    if([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
        mailCont.mailComposeDelegate = self;
        
        [mailCont setSubject:@"Document"];
        
        [mailCont setToRecipients:[NSArray arrayWithObject:@""]];
        [mailCont setMessageBody:@"Don't ever want to give you up" isHTML:NO];
        
        NSString *mimeType = @"image/png";
       
        
        [mailCont addAttachmentData:ImgData mimeType:mimeType fileName:@"barcode"];
        
        
        [self.shareDelegate sendMailCompose:mailCont];
        
    }
}

// Then implement the delegate method
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
   // [self dismissViewControllerAnimated:YES completion:nil];
    [self.shareDelegate closeMailComposeView];
}


@end
