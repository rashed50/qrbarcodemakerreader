//
//  Helper.swift
//  QRBarcodeProject
//
//  Created by Rashedul Hoque on 1/5/20.
//  Copyright © 2020 SoftKnight. All rights reserved.
//

import UIKit


class Helper: NSObject {

    @objc
    static func readBarcodeFromGalleryPhoto(img: UIImage) {

        let detector:CIDetector = CIDetector(ofType: CIDetectorTypeQRCode, context:nil, options:[CIDetectorAccuracy: CIDetectorAccuracyHigh])!
        let decode = ""
        let ciImage:CIImage = CIImage(image: img)!
        var message:String = "";

        let features = detector.features(in: ciImage)
        for feature in features as! [CIQRCodeFeature] {
            message += feature.messageString!
        }

        if(message == "") {
            print("nothing")

        } else {
            print("\(message)")
        }



    }
    
    
    //
    // MARK: SET NAVIGATION BAR PROPERTY
    //
       @objc
      static func setNavigationBarProperty(navbar:UINavigationController, size: Int ,title:String){
          
          let ssize  = CGFloat(size)
          let attrs = [
              NSAttributedString.Key.foregroundColor:UIColor.white,NSAttributedString.Key.font: UIFont(name: getAppTextFont(), size:ssize)!
           ]
          navbar.navigationBar.titleTextAttributes = attrs
          navbar.navigationBar.topItem?.title = title
          navbar.navigationBar.barTintColor = self.getAppColor() //089EAA
      }
      
      @objc
        static func setTabBarProperty(tabBar:UITabBarController){
          
          tabBar.tabBar.barTintColor = getAppColor()
          tabBar.tabBar.tintColor =  self.getAppTextColor()
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for:.normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor:getAppTextColor()], for:.selected)
        }
    

    static  func getAppTextFont() -> String {
        
        return "Georgia-Bold"
    }
    
   @objc static  func getAppColor() -> UIColor {

        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: "000000"))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        let color = UIColor(red: red, green: green, blue: blue, alpha: 1)
        return color
    }
    
   @objc static  func getAppTextColor() -> UIColor {

        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: "ffffff"))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0

        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: 1)
        return color
    }
    

 static func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        let scanner: Scanner = Scanner(string: hexStr)
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
    
    
  @objc  static func setUIButtonProperty(button:UIButton, title:String){
         
        button.backgroundColor = self.getAppColor()
        if title != ""{
            button.setTitle(title, for: .normal)
        }
        button.layer.cornerRadius = 20
        button.setTitleColor(self.getAppTextColor(), for: .normal)
    }
    
}
