//
//  ShareActionSheet.h
//  QR Code P2
//
//  Created by SoftKnight on 12/9/15.
//  Copyright © 2015 SoftKnight. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Social/Social.h>

#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>


#define APP_COLOR(r, g, b) \
[UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1]

@protocol SocialShareDelegate <NSObject>

-(void) socialShareViewOpen:(SLComposeViewController*) composeController;
-(void) socialShareViewClose: (SLComposeViewController*) composeController;
-(void) sendMailCompose:(MFMailComposeViewController *)mailCont;
-(void) closeMailComposeView;

@end

@interface ShareActionSheet : NSObject

@property id<SocialShareDelegate> shareDelegate;
- (void)ShareToSocialMedia:(int)shareType;

-(void)SendByEmail:(NSData*)ImgData;

@end
